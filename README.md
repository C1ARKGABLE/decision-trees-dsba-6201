# Decision Trees in Python

## This repo is a sample of what a non-pruned decision tree looks like!

## The final output:
![A pretty decision tree](output.png)

### Note: Do not use this code in production! 

### This was completed for a class homework assignment. Only use cost complexity pruned trees in production.
