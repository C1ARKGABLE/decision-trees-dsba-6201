# Install some packages to do stuff
import pandas as pd  # Excel/SQL style data tool... Really powerful
import matplotlib.pyplot as plt  # Plotting base
from sklearn.tree import DecisionTreeClassifier  # The classifier
from sklearn.tree import plot_tree  # Specific tree plotting tool

# Set the plotting size default
plt.figure(figsize=(15, 7.5))

# Read in the data as a DataFrame
df = pd.read_csv("data.csv")

# print the first 5 rows
print(df.head())

# Convert the data to true/false
# If Parents = "Yes"; Then Money = True; Else Money =  False
df["Parents"] = df["Parents"] == "Yes"
# If Money = "Rich"; Then Money = True; Else Money = False
df["Money"] = df["Money"] == "Rich"

# Seperate our classes (decisions) and our data
data = df.drop("Decision", axis=1).copy()
decisions = df["Decision"].copy()

# Get the dummy varaibles for the Weather column.
data_encoded = pd.get_dummies(data, columns=["Weather"])

# Alternatively, we could convert Weather types to integers with a dictionary
"""
weather_lookup = {
    "Rainy": 0,
    "Sunny": 1,
    "Windy": 2,
}
data["Weather"].replace(
    weather_lookup, inplace=True,
)
data_encoded = data
"""

# Create an empty tree object
tree = DecisionTreeClassifier(criterion="entropy")

# Fit the tree to our data (aka, do math and such)
tree = tree.fit(data_encoded, decisions)

# Plot the tree!
plot_tree(
    tree,
    filled=True,
    rounded=True,
    class_names=decisions.unique(),
    feature_names=data_encoded.columns,
)

# Show the plot
plt.show()
